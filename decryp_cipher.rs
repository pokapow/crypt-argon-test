use aes_gcm::{
    aead::{AeadInPlace, KeyInit},
    Aes256Gcm, Nonce,
};
use argon2::Argon2;
use base64::{engine::general_purpose, Engine as _};

fn main() {
    let iv = vec![169, 224, 176, 114, 58, 240, 180, 61, 152, 171, 142, 95];
    let cipher_text = "YnFb9sH+Qx/HbKM9pBXunFacoJFOyI/PbzP4UxZUaPRfahXDTlD1Se1M1Nya5pgQaSC1sRnR6Vif6sGK2qebhovU6x8ZwxA0atk9OH9yyQQL72iO9w2ykHEh";
    let pin = "1234";

    let plain_text = decrypt(cipher_text, pin, &iv);
    println!("Decrypted mnemonic: {}", plain_text);

    let expected_mnemonic =
        "abandon amount liar amount expire adjust cage candy arch gather drum buyer";
    if plain_text == expected_mnemonic {
        println!("Decrypted mnemonic matches the expected mnemonic.");
    } else {
        println!("Decrypted mnemonic does not match the expected mnemonic.");
    }
}

fn decrypt(cipher_text: &str, pin: &str, iv: &[u8]) -> String {
    let key = derive_key(pin, iv);
    let cipher = Aes256Gcm::new((&key).into());
    let nonce = Nonce::from_slice(iv);

    let mut buffer = general_purpose::STANDARD
        .decode(cipher_text)
        .expect("Failed to decode ciphertext");

    cipher
        .decrypt_in_place(&nonce, &[], &mut buffer)
        .expect("Decryption failed");

    String::from_utf8(buffer).expect("Failed to convert to string")
}

fn derive_key(password: &str, salt: &[u8]) -> [u8; 32] {
    let config = Argon2::new(
        argon2::Algorithm::Argon2id,
        argon2::Version::V0x13,
        argon2::Params::new(u32::pow(2, 16), 3, 4, Some(32)).unwrap(),
    );

    let mut hash_bytes = [0u8; 32];
    config
        .hash_password_into(password.as_bytes(), salt, &mut hash_bytes)
        .expect("Failed to derive key");
    hash_bytes
}
