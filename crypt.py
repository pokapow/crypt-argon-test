#!/usr/bin/env python3
import argparse
import base64
import os
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from argon2.low_level import hash_secret_raw, Type


def main():
    parser = argparse.ArgumentParser(
        description="Encrypt or decrypt text using AES-GCM and Argon2"
    )
    parser.add_argument(
        "-m",
        "--mode",
        choices=["encrypt", "decrypt"],
        required=True,
        help="Mode: encrypt or decrypt",
    )
    parser.add_argument("-p", "--pin", required=True, help="PIN code")
    parser.add_argument(
        "-t", "--text", required=True, help="Text to encrypt or decrypt"
    )

    args = parser.parse_args()

    iv = os.urandom(12)

    if args.mode == "encrypt":
        cipher_text = encrypt(args.text, args.pin, iv)
        iv_base64 = base64.b64encode(iv).decode("utf-8")
        print(iv_base64 + ":" + cipher_text)
    elif args.mode == "decrypt":
        parts = args.text.split(":")
        if len(parts) != 2:
            print("Invalid input. Expected format: <IV>:<ciphertext>")
            exit(1)
        iv_base64, cipher_text = parts
        iv = base64.b64decode(iv_base64)
        plain_text = decrypt(cipher_text, args.pin, iv)
        print(plain_text)


def encrypt(text, pin, iv):
    key = derive_key(pin, iv)
    aesgcm = AESGCM(key)
    cipher_text = aesgcm.encrypt(iv, text.encode("utf-8"), None)
    return base64.b64encode(cipher_text).decode("utf-8")


def decrypt(cipher_text, pin, iv):
    key = derive_key(pin, iv)
    aesgcm = AESGCM(key)
    cipher_data = base64.b64decode(cipher_text)
    plain_text = aesgcm.decrypt(iv, cipher_data, None)
    return plain_text.decode("utf-8")


def derive_key(pin, iv):
    return hash_secret_raw(
        secret=pin.encode(),
        salt=iv,
        time_cost=3,
        memory_cost=2**16,
        parallelism=4,
        hash_len=32,
        type=Type.ID,
    )


if __name__ == "__main__":
    main()
