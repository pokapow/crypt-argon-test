use aes_gcm::{
    aead::{AeadInPlace, KeyInit},
    Aes256Gcm, Nonce,
};
use argon2::Argon2;
use base64::{engine::general_purpose, Engine as _};
use clap::Parser;
use rand::Rng;

#[derive(Parser)]
#[clap(version, about)]
struct Args {
    #[clap(short, long, value_parser, help = "Mode: encrypt or decrypt")]
    mode: String,
    #[clap(short, long, value_parser, help = "PIN code")]
    pin: String,
    #[clap(short, long, value_parser, help = "Text to encrypt or decrypt")]
    text: String,
}

fn main() {
    let args = Args::parse();

    let iv = create_random_bytes(12);

    if args.mode == "encrypt" {
        let cipher_text = encrypt(&args.text, &args.pin, &iv);
        let iv_base64 = general_purpose::STANDARD.encode(iv);
        println!("{}:{}", iv_base64, cipher_text);
    } else if args.mode == "decrypt" {
        let parts: Vec<&str> = args.text.split(':').collect();
        if parts.len() != 2 {
            println!("Invalid input. Expected format: <IV>:<ciphertext>");
            std::process::exit(1);
        }
        let iv_base64 = parts[0];
        let cipher_text = parts[1];
        let iv = general_purpose::STANDARD
            .decode(iv_base64)
            .expect("Failed to decode IV");
        let plain_text = decrypt(cipher_text, &args.pin, &iv);
        println!("{}", plain_text);
    } else {
        println!("Invalid mode. Use 'encrypt' or 'decrypt'.");
        std::process::exit(1);
    }
}

fn encrypt(text: &str, pin: &str, iv: &[u8]) -> String {
    let key = derive_key(pin, iv);
    let cipher = Aes256Gcm::new((&key).into());
    let nonce = Nonce::from_slice(iv);

    let mut buffer = vec![0u8; text.len() + 16];
    buffer[..text.len()].copy_from_slice(text.as_bytes());
    cipher
        .encrypt_in_place(&nonce, &[], &mut buffer)
        .expect("Encryption failed");

    general_purpose::STANDARD.encode(buffer)
}

fn decrypt(cipher_text: &str, pin: &str, iv: &[u8]) -> String {
    let key = derive_key(pin, iv);
    let cipher = Aes256Gcm::new((&key).into());
    let nonce = Nonce::from_slice(iv);

    let mut buffer = general_purpose::STANDARD
        .decode(cipher_text)
        .expect("Failed to decode ciphertext");

    cipher
        .decrypt_in_place(&nonce, &[], &mut buffer)
        .expect("Decryption failed");

    String::from_utf8(buffer).expect("Failed to convert to string")
}

fn derive_key(password: &str, salt: &[u8]) -> [u8; 32] {
    let config = Argon2::new(
        argon2::Algorithm::Argon2id,
        argon2::Version::V0x13,
        argon2::Params::new(u32::pow(2, 16), 3, 4, Some(32)).unwrap(),
    );

    let mut hash_bytes = [0u8; 32];
    config
        .hash_password_into(password.as_bytes(), salt, &mut hash_bytes)
        .expect("Failed to derive key");
    hash_bytes
}

fn create_random_bytes(length: usize) -> Vec<u8> {
    let mut rng = rand::thread_rng();
    (0..length).map(|_| rng.gen()).collect()
}
