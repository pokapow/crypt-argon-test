#!/bin/bash

# Mnemonic et code PIN
mnemonic="abandon amount liar amount expire adjust cage candy arch gather drum buyer"
pin="1234"

# Fonction pour comparer Python et Dart
compare_python_dart() {
    encrypted=$(./crypt.py -m encrypt -p "$pin" -t "$mnemonic")
    decrypted=$(dart crypt.dart -m decrypt -p "$pin" -t "$encrypted")
    encrypted_dart=$(dart crypt.dart -m encrypt -p "$pin" -t "$decrypted")
    decrypted_python=$(./crypt.py -m decrypt -p "$pin" -t "$encrypted_dart")

    echo "Mnemonic original : $mnemonic"
    echo "Mnemonic décrypté : $decrypted_python"

    if [ "$mnemonic" = "$decrypted_python" ]; then
        echo "Le mnemonic décrypté correspond à l'original."
    else
        echo "Le mnemonic décrypté ne correspond pas à l'original."
    fi
}

# Fonction pour comparer Rust et Dart
compare_rust_dart() {
    encrypted=$(cargo run --quiet -- -m encrypt -p "$pin" -t "$mnemonic" | tr -d '\0')
    decrypted=$(dart crypt.dart -m decrypt -p "$pin" -t "$encrypted" | tr -d '\0')
    encrypted_dart=$(dart crypt.dart -m encrypt -p "$pin" -t "$decrypted" | tr -d '\0')
    decrypted_rust=$(cargo run --quiet -- -m decrypt -p "$pin" -t "$encrypted_dart" | tr -d '\0')

    echo "Mnemonic original : $mnemonic"
    echo "Mnemonic décrypté : $decrypted_rust"

    if [ "$mnemonic" = "$decrypted_rust" ]; then
        echo "Le mnemonic décrypté correspond à l'original."
    else
        echo "Le mnemonic décrypté ne correspond pas à l'original."
    fi
}


# Vérification des arguments
if [ $# -ne 1 ]; then
    echo "Usage: $0 [python|rust]"
    exit 1
fi

# Choix de l'implémentation à comparer
case $1 in
    python)
        compare_python_dart
        ;;
    rust)
        compare_rust_dart
        ;;
    *)
        echo "Argument invalide. Utilisez 'python' ou 'rust'."
        exit 1
        ;;
esac
