import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:args/args.dart';
import 'package:pointycastle/export.dart';

Future<void> main(List<String> arguments) async {
  final parser = ArgParser()
    ..addOption('mode',
        abbr: 'm',
        allowed: ['encrypt', 'decrypt'],
        help: 'Mode: encrypt or decrypt')
    ..addOption('pin', abbr: 'p', help: 'PIN code')
    ..addOption('text', abbr: 't', help: 'Text to encrypt or decrypt');

  final args = parser.parse(arguments);
  final mode = args['mode'] as String?;
  final pin = args['pin'] as String?;
  final text = args['text'] as String?;

  if (mode == null || pin == null || text == null) {
    print('Usage: dart crypt.dart -m <mode> -p <pin> -t <text>');
    exit(1);
  }

  final iv = _createRandomBytes(12);

  if (mode == 'encrypt') {
    final cipherText = await _encrypt(text, pin, iv);
    final ivBase64 = base64.encode(iv);
    print('$ivBase64:$cipherText');
  } else if (mode == 'decrypt') {
    final parts = text.split(':');
    if (parts.length != 2) {
      print('Invalid input. Expected format: <IV>:<ciphertext>');
      exit(1);
    }
    final ivBase64 = parts[0];
    final cipherText = parts[1];
    final iv = base64.decode(ivBase64);
    final plainText = await _decrypt(cipherText, pin, Uint8List.fromList(iv));
    print(plainText);
  }
}

Future<String> _encrypt(String text, String pin, Uint8List iv) async {
  final keyDerivator = await _createKeyDerivator(pin, iv);
  final key = keyDerivator.process(utf8.encode(pin));

  final cipher = GCMBlockCipher(AESEngine());
  final params = AEADParameters(KeyParameter(key), 128, iv, Uint8List(0));

  cipher.init(true, params);

  final input = Uint8List.fromList(utf8.encode(text));
  final output = Uint8List(cipher.getOutputSize(input.length));
  var len = cipher.processBytes(input, 0, input.length, output, 0);
  len += cipher.doFinal(output, len);

  return base64.encode(output.sublist(0, len));
}

Future<String> _decrypt(String cipherText, String pin, Uint8List iv) async {
  final keyDerivator = await _createKeyDerivator(pin, iv);
  final key = keyDerivator.process(utf8.encode(pin));

  final cipher = GCMBlockCipher(AESEngine());
  final params = AEADParameters(KeyParameter(key), 128, iv, Uint8List(0));

  cipher.init(false, params);
  final cipherData = base64.decode(cipherText);

  final output = Uint8List(cipher.getOutputSize(cipherData.length));
  var len = cipher.processBytes(cipherData, 0, cipherData.length, output, 0);
  len += cipher.doFinal(output, len);

  return utf8.decode(output.sublist(0, len));
}

Future<Argon2BytesGenerator> _createKeyDerivator(
    String password, Uint8List salt) async {
  final argon2 = Argon2BytesGenerator();
  argon2.init(
    Argon2Parameters(
      Argon2Parameters.ARGON2_id,
      salt,
      // secret: utf8.encode(password),
      desiredKeyLength: 32,
      iterations: 3,
      memoryPowerOf2: 16,
      lanes: 4,
      version: Argon2Parameters.ARGON2_VERSION_13,
    ),
  );
  return argon2;
}

Uint8List _createRandomBytes(int length) {
  final secureRandom = FortunaRandom();
  final seedSource = Random.secure();
  final seeds = <int>[];
  for (int i = 0; i < 32; i++) {
    seeds.add(seedSource.nextInt(255));
  }
  secureRandom.seed(KeyParameter(Uint8List.fromList(seeds)));

  return secureRandom.nextBytes(length);
}
